package com.fishexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FishexamApplication {
    public static void main(String[] args) {
        SpringApplication.run(FishexamApplication.class, args);
    }

}
