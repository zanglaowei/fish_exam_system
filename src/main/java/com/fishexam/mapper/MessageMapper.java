package com.fishexam.mapper;

import com.fishexam.pojo.MessagePojo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: FishExam
 * @description: Message dao 层
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-24 23:54
 **/
@Mapper
@Repository
public interface MessageMapper {
    List<MessagePojo> selectMessage();

    int updateStatus(int status);
}
