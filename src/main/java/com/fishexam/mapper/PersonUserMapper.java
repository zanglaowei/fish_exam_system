package com.fishexam.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: FishExam
 * @description: 医院成员管理
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-23 10:01
 **/
@Mapper
@Repository
public interface PersonUserMapper {
    List<PersonUserMapper> selectPerson();

    String selectPasswordByName(String user_username);

    String selectPasswordByNameEmailPhone(String user_username);
}
