package com.fishexam.pojo;

/**
 * @program: FishExam
 * @description: 医院用户表
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-23 10:12
 **/
public class PersonUserPojo {
    private int userid;
    private String username;
    private String password;
    private String phone;
    private String email;
    private String post;
    private String purview;

    public PersonUserPojo() {
        super();
    }

    public PersonUserPojo(int userid, String username, String password, String phone, String email, String post, String purview) {
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.post = post;
        this.purview = purview;
    }

    @Override
    public String toString() {
        return "PersonUserPojo{" +
                "userid=" + userid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", post='" + post + '\'' +
                ", purview='" + purview + '\'' +
                '}';
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPurview() {
        return purview;
    }

    public void setPurview(String purview) {
        this.purview = purview;
    }
}
