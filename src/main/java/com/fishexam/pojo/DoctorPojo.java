package com.fishexam.pojo;

/**
 * @program: FishExam
 * @description:
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-26 08:41
 **/
public class DoctorPojo {
    private int doctor_id;
    private String doctor_number;
    private String doctor_name;
    private String doctor_photo;
    private String doctor_post;
    private String doctor_email;
    private String doctor_phone;
    private int doctor_age;
    private int doctor_gender;
    private String doctor_level;
    private String doctor_demo1;
    private String doctor_demo2;
    private String doctor_demo3;

    @Override
    public String toString() {
        return "DoctorPojo{" +
                "doctor_id=" + doctor_id +
                ", doctor_number='" + doctor_number + '\'' +
                ", doctor_name='" + doctor_name + '\'' +
                ", doctor_photo='" + doctor_photo + '\'' +
                ", doctor_post='" + doctor_post + '\'' +
                ", doctor_email='" + doctor_email + '\'' +
                ", doctor_phone='" + doctor_phone + '\'' +
                ", doctor_age=" + doctor_age +
                ", doctor_gender=" + doctor_gender +
                ", doctor_level='" + doctor_level + '\'' +
                ", doctor_demo1='" + doctor_demo1 + '\'' +
                ", doctor_demo2='" + doctor_demo2 + '\'' +
                ", doctor_demo3='" + doctor_demo3 + '\'' +
                '}';
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getDoctor_number() {
        return doctor_number;
    }

    public void setDoctor_number(String doctor_number) {
        this.doctor_number = doctor_number;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDoctor_photo() {
        return doctor_photo;
    }

    public void setDoctor_photo(String doctor_photo) {
        this.doctor_photo = doctor_photo;
    }

    public String getDoctor_post() {
        return doctor_post;
    }

    public void setDoctor_post(String doctor_post) {
        this.doctor_post = doctor_post;
    }

    public String getDoctor_email() {
        return doctor_email;
    }

    public void setDoctor_email(String doctor_email) {
        this.doctor_email = doctor_email;
    }

    public String getDoctor_phone() {
        return doctor_phone;
    }

    public void setDoctor_phone(String doctor_phone) {
        this.doctor_phone = doctor_phone;
    }

    public int getDoctor_age() {
        return doctor_age;
    }

    public void setDoctor_age(int doctor_age) {
        this.doctor_age = doctor_age;
    }

    public int getDoctor_gender() {
        return doctor_gender;
    }

    public void setDoctor_gender(int doctor_gender) {
        this.doctor_gender = doctor_gender;
    }

    public String getDoctor_level() {
        return doctor_level;
    }

    public void setDoctor_level(String doctor_level) {
        this.doctor_level = doctor_level;
    }

    public String getDoctor_demo1() {
        return doctor_demo1;
    }

    public void setDoctor_demo1(String doctor_demo1) {
        this.doctor_demo1 = doctor_demo1;
    }

    public String getDoctor_demo2() {
        return doctor_demo2;
    }

    public void setDoctor_demo2(String doctor_demo2) {
        this.doctor_demo2 = doctor_demo2;
    }

    public String getDoctor_demo3() {
        return doctor_demo3;
    }

    public void setDoctor_demo3(String doctor_demo3) {
        this.doctor_demo3 = doctor_demo3;
    }

    public DoctorPojo(int doctor_id, String doctor_number, String doctor_name, String doctor_photo, String doctor_post, String doctor_email, String doctor_phone, int doctor_age, int doctor_gender, String doctor_level, String doctor_demo1, String doctor_demo2, String doctor_demo3) {
        this.doctor_id = doctor_id;
        this.doctor_number = doctor_number;
        this.doctor_name = doctor_name;
        this.doctor_photo = doctor_photo;
        this.doctor_post = doctor_post;
        this.doctor_email = doctor_email;
        this.doctor_phone = doctor_phone;
        this.doctor_age = doctor_age;
        this.doctor_gender = doctor_gender;
        this.doctor_level = doctor_level;
        this.doctor_demo1 = doctor_demo1;
        this.doctor_demo2 = doctor_demo2;
        this.doctor_demo3 = doctor_demo3;
    }

    public DoctorPojo() {
        super();
    }
}
