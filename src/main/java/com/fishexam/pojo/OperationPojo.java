package com.fishexam.pojo;

import java.util.Date;

/**
 * @program: FishExam
 * @description:
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-26 08:01
 **/
public class OperationPojo {
    private int operation_id;

    private String operation_number;

    private String operation_10;

    private String operation_11;

    private String operation_12;

    private String operation_2;

    private String operation_3;

    private String operation_4;

    private String operation_5;

    private String operation_6;

    private Date operation_date;

    private String operation_what;

    @Override
    public String toString() {
        return "operation{" +
                "operation_id=" + operation_id +
                ", operation_number='" + operation_number + '\'' +
                ", operation_10='" + operation_10 + '\'' +
                ", operation_11='" + operation_11 + '\'' +
                ", operation_12='" + operation_12 + '\'' +
                ", operation_2='" + operation_2 + '\'' +
                ", operation_3='" + operation_3 + '\'' +
                ", operation_4='" + operation_4 + '\'' +
                ", operation_5='" + operation_5 + '\'' +
                ", operation_6='" + operation_6 + '\'' +
                ", operation_date=" + operation_date +
                ", operation_what='" + operation_what + '\'' +
                '}';
    }

    public OperationPojo() {
        super();
    }

    public OperationPojo(int operation_id, String operation_number, String operation_10, String operation_11, String operation_12, String operation_2, String operation_3, String operation_4, String operation_5, String operation_6, Date operation_date, String operation_what) {
        this.operation_id = operation_id;
        this.operation_number = operation_number;
        this.operation_10 = operation_10;
        this.operation_11 = operation_11;
        this.operation_12 = operation_12;
        this.operation_2 = operation_2;
        this.operation_3 = operation_3;
        this.operation_4 = operation_4;
        this.operation_5 = operation_5;
        this.operation_6 = operation_6;
        this.operation_date = operation_date;
        this.operation_what = operation_what;
    }

    public int getOperation_id() {
        return operation_id;
    }

    public void setOperation_id(int operation_id) {
        this.operation_id = operation_id;
    }

    public String getOperation_number() {
        return operation_number;
    }

    public void setOperation_number(String operation_number) {
        this.operation_number = operation_number;
    }

    public String getOperation_10() {
        return operation_10;
    }

    public void setOperation_10(String operation_10) {
        this.operation_10 = operation_10;
    }

    public String getOperation_11() {
        return operation_11;
    }

    public void setOperation_11(String operation_11) {
        this.operation_11 = operation_11;
    }

    public String getOperation_12() {
        return operation_12;
    }

    public void setOperation_12(String operation_12) {
        this.operation_12 = operation_12;
    }

    public String getOperation_2() {
        return operation_2;
    }

    public void setOperation_2(String operation_2) {
        this.operation_2 = operation_2;
    }

    public String getOperation_3() {
        return operation_3;
    }

    public void setOperation_3(String operation_3) {
        this.operation_3 = operation_3;
    }

    public String getOperation_4() {
        return operation_4;
    }

    public void setOperation_4(String operation_4) {
        this.operation_4 = operation_4;
    }

    public String getOperation_5() {
        return operation_5;
    }

    public void setOperation_5(String operation_5) {
        this.operation_5 = operation_5;
    }

    public String getOperation_6() {
        return operation_6;
    }

    public void setOperation_6(String operation_6) {
        this.operation_6 = operation_6;
    }

    public Date getOperation_date() {
        return operation_date;
    }

    public void setOperation_date(Date operation_date) {
        this.operation_date = operation_date;
    }

    public String getOperation_what() {
        return operation_what;
    }

    public void setOperation_what(String operation_what) {
        this.operation_what = operation_what;
    }
}
