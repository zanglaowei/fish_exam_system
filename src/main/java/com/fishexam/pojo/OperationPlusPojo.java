package com.fishexam.pojo;

import java.util.Date;

/**
 * @program: FishExam
 * @description:
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-26 08:57
 **/
public class OperationPlusPojo {
    private int operationplus_id;
    private String operationplus_number;
    private Date operationplus_date;
    private String operationplus_doctor;
    private String operationplus_msg;
    private String operationplus_demo1;
    private String operationplus_demo2;
    private String operationplus_demo3;

    @Override
    public String toString() {
        return "OperationPlusPojo{" +
                "operationplus_id=" + operationplus_id +
                ", operationplus_number='" + operationplus_number + '\'' +
                ", operationplus_date=" + operationplus_date +
                ", operationplus_doctor='" + operationplus_doctor + '\'' +
                ", operationplus_msg='" + operationplus_msg + '\'' +
                ", operationplus_demo1='" + operationplus_demo1 + '\'' +
                ", operationplus_demo2='" + operationplus_demo2 + '\'' +
                ", operationplus_demo3='" + operationplus_demo3 + '\'' +
                '}';
    }

    public OperationPlusPojo() {
        super();
    }

    public OperationPlusPojo(int operationplus_id, String operationplus_number, Date operationplus_date, String operationplus_doctor, String operationplus_msg, String operationplus_demo1, String operationplus_demo2, String operationplus_demo3) {
        this.operationplus_id = operationplus_id;
        this.operationplus_number = operationplus_number;
        this.operationplus_date = operationplus_date;
        this.operationplus_doctor = operationplus_doctor;
        this.operationplus_msg = operationplus_msg;
        this.operationplus_demo1 = operationplus_demo1;
        this.operationplus_demo2 = operationplus_demo2;
        this.operationplus_demo3 = operationplus_demo3;
    }

    public int getOperationplus_id() {
        return operationplus_id;
    }

    public void setOperationplus_id(int operationplus_id) {
        this.operationplus_id = operationplus_id;
    }

    public String getOperationplus_number() {
        return operationplus_number;
    }

    public void setOperationplus_number(String operationplus_number) {
        this.operationplus_number = operationplus_number;
    }

    public Date getOperationplus_date() {
        return operationplus_date;
    }

    public void setOperationplus_date(Date operationplus_date) {
        this.operationplus_date = operationplus_date;
    }

    public String getOperationplus_doctor() {
        return operationplus_doctor;
    }

    public void setOperationplus_doctor(String operationplus_doctor) {
        this.operationplus_doctor = operationplus_doctor;
    }

    public String getOperationplus_msg() {
        return operationplus_msg;
    }

    public void setOperationplus_msg(String operationplus_msg) {
        this.operationplus_msg = operationplus_msg;
    }

    public String getOperationplus_demo1() {
        return operationplus_demo1;
    }

    public void setOperationplus_demo1(String operationplus_demo1) {
        this.operationplus_demo1 = operationplus_demo1;
    }

    public String getOperationplus_demo2() {
        return operationplus_demo2;
    }

    public void setOperationplus_demo2(String operationplus_demo2) {
        this.operationplus_demo2 = operationplus_demo2;
    }

    public String getOperationplus_demo3() {
        return operationplus_demo3;
    }

    public void setOperationplus_demo3(String operationplus_demo3) {
        this.operationplus_demo3 = operationplus_demo3;
    }
}
