package com.fishexam.pojo;

import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * @program: FishExam
 * @description: 宠物成员
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-23 10:21
 **/
public class PetsUserPojo {
    private int petsid;
    private String number;
    private String name;
    private int gender;
    private String status;
    private Date date;
    private int bed;
    private String age;
    private String names;

    public PetsUserPojo() {
        super();
    }

    @Override
    public String toString() {
        return "PetsUserPojo{" +
                "petsid=" + petsid +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", status='" + status + '\'' +
                ", date=" + date +
                ", bed=" + bed +
                ", age='" + age + '\'' +
                ", names='" + names + '\'' +
                '}';
    }

    public PetsUserPojo(int petsid, String number, String name, int gender, String status, Date date, int bed, String age, String names) {
        this.petsid = petsid;
        this.number = number;
        this.name = name;
        this.gender = gender;
        this.status = status;
        this.date = date;
        this.bed = bed;
        this.age = age;
        this.names = names;
    }

    public int getPetsid() {
        return petsid;
    }

    public void setPetsid(int petsid) {
        this.petsid = petsid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getBed() {
        return bed;
    }

    public void setBed(int bed) {
        this.bed = bed;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }
}
