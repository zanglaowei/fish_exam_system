package com.fishexam.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: FishExam
 * @description: message的pojo层
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-24 23:44
 **/

public class MessagePojo {
    private int message_id;
    private String message_name;
    private String message_photo;
    private String message_daytime;
    private String message_msg;
    private int message_doctorid;
    private int message_status;
    private Date message_datetime;

    @Override
    public String toString() {
        return "MessagePojo{" +
                "message_id=" + message_id +
                ", message_name='" + message_name + '\'' +
                ", message_photo='" + message_photo + '\'' +
                ", message_daytime='" + message_daytime + '\'' +
                ", message_msg='" + message_msg + '\'' +
                ", message_doctorid=" + message_doctorid +
                ", message_status=" + message_status +
                ", message_datetime=" + message_datetime +
                '}';
    }

    public MessagePojo(int message_id, String message_name, String message_photo,
                       String message_daytime, String message_msg, int message_doctorid,
                       int message_status) {
        this.message_id = message_id;
        this.message_name = message_name;
        this.message_photo = message_photo;
        this.message_daytime = message_daytime;
        this.message_msg = message_msg;
        this.message_doctorid = message_doctorid;
        this.message_status = message_status;
        this.message_datetime = new Date();
    }

    public MessagePojo() {
        super();
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public String getMessage_name() {
        return message_name;
    }

    public void setMessage_name(String message_name) {
        this.message_name = message_name;
    }

    public String getMessage_photo() {
        return message_photo;
    }

    public void setMessage_photo(String message_photo) {
        this.message_photo = message_photo;
    }

    public String getMessage_daytime() {
        return message_daytime;
    }

    public void setMessage_daytime(String message_daytime) {
        this.message_daytime = message_daytime;
    }

    public String getMessage_msg() {
        return message_msg;
    }

    public void setMessage_msg(String message_msg) {
        this.message_msg = message_msg;
    }

    public int getMessage_doctorid() {
        return message_doctorid;
    }

    public void setMessage_doctorid(int message_doctorid) {
        this.message_doctorid = message_doctorid;
    }

    public int getMessage_status() {
        return message_status;
    }

    public void setMessage_status(int message_status) {
        this.message_status = message_status;
    }

    public Date getMessage_datetime() {
        return message_datetime;
    }

    public void setMessage_datetime(Date message_datetime) {
        this.message_datetime = message_datetime;
    }
}
