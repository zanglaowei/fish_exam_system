package com.fishexam.service.impl;

import com.fishexam.mapper.PersonUserMapper;
import com.fishexam.service.PersonUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: FishExam
 * @description: PersonUserService实现类
 * @author: Zhu_Fish
 * @email: 1766722033@qq.com
 * @create: 2020-05-23 10:43
 **/
@Service
public class PersonUserServiceImpl implements PersonUserService {
    @Autowired
    PersonUserMapper personUserMapper;


    @Override
    public List<PersonUserMapper> selectPerson() {
        return personUserMapper.selectPerson();
    }

    @Override
    public String selectPasswordByName(String user_username) {
        return personUserMapper.selectPasswordByName(user_username);
    }

    @Override
    public String selectPasswordByNameEmailPhone(String user_username) {
        return personUserMapper.selectPasswordByNameEmailPhone(user_username);
    }
}
