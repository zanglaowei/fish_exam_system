package com.fishexam.util;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Component;

@Component
public class PasswordLock {
    public String passwordLocks(String password) {
        String algorithmName = "md5";
        SimpleHash simpleHash = new SimpleHash(algorithmName, password, "fish", 3);
        String simpleHashed = String.valueOf(simpleHash);
        return simpleHashed;
    }
}
